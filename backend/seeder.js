import dotenv from "dotenv";
import colors from "colors";

import connectDb from "./config/db.js";

import User from "./models/userModel.js";
import users from "./data/users.js";

import Product from "./models/productModel.js";
import products from "./data/products.js";

import Order from "./models/orderModel.js";

dotenv.config();

connectDb();

const importData = async () => {
  try {
    // Deletes everything
    await Order.deleteMany();
    await Product.deleteMany();
    await User.deleteMany();

    const createdUsers = await User.insertMany(users);
    const adminUser = createdUsers[0]._id; // The first user is the admin

    const sampleProducts = products.map((product) => {
      return { ...product, user: adminUser };
    });

    await Product.insertMany(sampleProducts);

    console.log("Data Imported".green.inverse);
    process.exit();
  } catch (err) {
    console.log(`${error}`.red.inverse);
    process.exit(1);
  }
};

const destroyData = async () => {
  try {
    // Deletes everything
    await Order.deleteMany();
    await Product.deleteMany();
    await User.deleteMany();

    console.log("Data Destroyed".red.inverse);
    process.exit();
  } catch (err) {
    console.log(`${error}`.red.inverse);
    process.exit(1);
  }
};

if (process.argv[2] === "-d") {
  destroyData();
} else {
  importData();
}
