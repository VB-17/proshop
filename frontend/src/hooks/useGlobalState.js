import React from "react";
import { useDispatch, useSelector } from "react-redux";

function useGlobalState(stateItem) {
  const dispatch = useDispatch();
  const data = useSelector((state) => state[stateItem]);

  return [dispatch, data];
}

export default useGlobalState;
