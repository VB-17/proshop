import React, { useEffect } from "react";
import useGlobalState from "../hooks/useGlobalState";
import { useQuery } from "react-query";

import { Col, Row } from "react-bootstrap";

import { listProducts } from "../redux/actions/productActions";

import Product from "../components/Product";
import Loader from "../components/Loader";
import Message from "../components/Message";
import axios from "axios";

function HomeScreen() {
  // const [dispatch, { loading, error, products }] =
  //   useGlobalState("productList");

  // useEffect(() => {
  //   dispatch(listProducts());
  // }, [dispatch]);

  const {
    isLoading: loading,
    error,
    data,
  } = useQuery("products", () => axios.get("/api/products"));

  return (
    <>
      <h1>Latest Products</h1>
      {loading ? (
        <Loader />
      ) : error ? (
        <Message variant="danger">{error}</Message>
      ) : (
        <Row>
          {data.data.map((product) => (
            <Col key={product._id} sm={12} md={6} lg={4} xl={3}>
              <Product product={product} />
            </Col>
          ))}
        </Row>
      )}
    </>
  );
}

export default HomeScreen;
