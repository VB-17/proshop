import React, { useEffect, useState } from "react";
import useGlobalState from "../hooks/useGlobalState";

import { useQuery } from "react-query";

import Rating from "../components/Rating";

import { Link, useParams } from "react-router-dom";
import {
  Row,
  Col,
  Image,
  ListGroup,
  Card,
  Button,
  Form,
} from "react-bootstrap";

import { listProductDetail } from "../redux/actions/productActions";

import Loader from "../components/Loader";
import Message from "../components/Message";
import axios from "axios";

function ProductScreen({ history }) {
  const { id } = useParams();
  const [qty, setQty] = useState(1);

  // const [dispatch, { loading, error, product }] =
  //   useGlobalState("productDetails");

  // useEffect(() => {
  //   dispatch(listProductDetail(id));
  // }, [dispatch, id]);

  const { isLoading, error, data } = useQuery(["products", id], () =>
    axios.get(`/api/products/${id}`)
  );

  console.log(data);

  const addToCartHandler = () => {
    history.push(`/cart/${id}?qty=${qty}`);
  };

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : error ? (
        <Message variant="danger">{error}</Message>
      ) : (
        <>
          <Link className="btn btn-light rounded my-3" to="/">
            <span style={{ display: "flex", alignItems: "center" }}>
              <i className="far fa-arrow-alt-circle-left fa-2x px-2"></i>Go Back
            </span>
          </Link>
          <Row>
            <Col md={6}>
              <Image src={data.data.image} alt={data.data.name} fluid />
            </Col>
            <Col md={3}>
              <ListGroup variant="flush">
                <ListGroup.Item>
                  <h3>{data.data.name}</h3>
                </ListGroup.Item>
                <ListGroup.Item
                  style={{ display: "flex", alignItems: "center" }}
                >
                  <Rating
                    value={data.data.rating}
                    text={`(${data.data.numReviews})`}
                  />
                </ListGroup.Item>
                <ListGroup.Item>Price: ${data.data.price}</ListGroup.Item>
                <ListGroup.Item>{data.data.description}</ListGroup.Item>
              </ListGroup>
            </Col>

            <Col md={3}>
              <Card>
                <ListGroup variant="flush">
                  <ListGroup.Item>
                    <Row>
                      <Col>Price:</Col>
                      <Col>
                        <strong>${data.data.price}</strong>
                      </Col>
                    </Row>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <Row>
                      <Col>Status:</Col>
                      <Col>
                        {data.data.countInStock > 0
                          ? "In Stock"
                          : "Out of Stock"}
                      </Col>
                    </Row>
                  </ListGroup.Item>
                  {data.data.countInStock > 0 && (
                    <ListGroup.Item>
                      <Row>
                        <Col>Qty</Col>
                        <Col>
                          <Form.Control
                            as="select"
                            value={qty}
                            onChange={(e) => setQty(e.target.value)}
                          >
                            {[...Array(data.data.countInStock).keys()].map(
                              (x) => (
                                <option key={x + 1} value={x + 1}>
                                  {x + 1}
                                </option>
                              )
                            )}
                          </Form.Control>
                        </Col>
                      </Row>
                    </ListGroup.Item>
                  )}
                  <ListGroup.Item>
                    <Button
                      onClick={addToCartHandler}
                      className="btn-block"
                      type="button"
                      disabled={data.data.countInStock === 0}
                    >
                      Add to Cart
                    </Button>
                  </ListGroup.Item>
                </ListGroup>
              </Card>
            </Col>
          </Row>
        </>
      )}
    </>
  );
}

export default ProductScreen;
